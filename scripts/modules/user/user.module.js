var userModule = angular.module('user.module', []);

/**********route config start**********/
userModule.config(function ($stateProvider) {
    $stateProvider
    .state('users', {
        abstract: false,
        url: "/users",
        templateUrl: "views/common/content.html"
    })
    .state('login', {
        url: "/login",
        templateUrl: "views/user/login.html",
        controller: 'LoginCtrl',
        data: { pageTitle: 'Login', specialClass: 'gray-bg' }
    })
    .state('register', {
        url: "/register",
        templateUrl: "views/user/register.html",
        controller: 'RegisterCtrl',
        data: { pageTitle: 'Register', specialClass: 'gray-bg' }
    })
    
    .state('forgot_password', {
        url: "/forgot_password",
        templateUrl: "views/user/forgot_password.html",
        data: { pageTitle: 'Forgot password', specialClass: 'gray-bg' }
    })
    .state('users.updateProfile', {
        url: "/updateProfile/:id",
        controller: 'UpdateProfileCtrl',
        templateUrl: "views/user/updateProfile.html",
        data: { pageTitle: 'Profile' },
        resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
    });
});
/**********route config end**********/


