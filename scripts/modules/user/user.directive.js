userModule.directive("passwordVerify", function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;

            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue; 
            }                    
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
   };
})
.directive('noSpecialChar', function() {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function(scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function(inputValue) {
          if (inputValue == null)
            return ''
          cleanInputValue = inputValue.replace(/[^\w\s]/gi, '');
          if (cleanInputValue != inputValue) {
            modelCtrl.$setViewValue(cleanInputValue);
            modelCtrl.$render();
          }
          return cleanInputValue;
        });
      }
    }
  })
.directive('loading', function () {
      return {
        restrict: 'E',
        replace:true,
        template: '<div class="loading"><img src="img/ajax-loader.gif" width="20" height="20" />LOADING...</div>',
        link: function (scope, element, attr) {
              scope.$watch('loading', function (val) {
                  if (val)
                      $(element).show();
                  else
                      $(element).hide();
              });
        }
      }
  })
.directive('captcha', function () {
    return {
      restrict : 'E',
            replace  : 'ture',
            link     : function (scope, elem, attrs) {
             scope.a    = Math.ceil(Math.random() * 10);
          scope.b    = Math.ceil(Math.random() * 10);       
          scope.c    = scope.a + scope.b;
          scope.resCd   = md5(scope.a + scope.b);
            },
      template : function (scope, element, attrs) {
       return  '<div class="captchaBlock">'+
          'Are you human?&nbsp;&nbsp;'+
          'What is {{a}} + {{b}} ? '+
                         '<input type="text" maxlength="2" size="2" placeholder="" name="captchaCode" data-ng-model="authorizationForm.captchaCode" required autocomplete="off"/>'+
                         '<input type="hidden" data-ng-model="resCd" value="{{resCd}}"/>'
                        '</div>';
      }
     };
});