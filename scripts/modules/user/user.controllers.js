userModule
.controller('RegisterCtrl', function($scope, $formSubmit, $timeout){
    /***********update profile start***************/
        $scope.regForm = function(form) {
            $scope.loading = true; 
            if ($scope.reg_form.$valid) {
                // Submit as normal
                var regParam    = {
                        'name'      : form.name,
                        'email'     : form.email,
                        'password'  : form.password
                }
                $formSubmit.registerUserFactory(regParam).then(function (regResponseData) {
                    console.log(regResponseData);
                    if(regResponseData.responseCode == 200){
                        $scope.Message = regResponseData.responseMessage;
                        form.name = '';
                        form.email = '';
                        form.password = '';
                        $timeout(function () {
                                $scope.loading = false;
                        }, 500);
                    } else if(regResponseData.responseCode == 201){
                        $scope.Message = regResponseData.responseMessage;
                        $timeout(function () {
                                $scope.loading = false;
                        }, 500);
                    } else if(regResponseData.responseCode == 202){
                        $scope.Message = regResponseData.responseMessage;
                        $timeout(function () {
                                $scope.loading = false;
                        }, 500);
                    } else if(regResponseData.responseCode == 203){
                        var arrData = regResponseData.responseMessage;
                        var mess = [];
                        for (var i = 0; i < arrData.length; ++i){
                           mess[i] = arrData[i].responseMessage; 
                        }
                         var arrToStr = mess.toString();
                        // console.log(arrToStr);
                        $scope.Message = arrToStr;
                        $timeout(function () {
                                $scope.loading = false;
                        }, 500);
                    }
                }, function (regResponseData) {
                    console.log('error');
                });
            } else {
                $scope.reg_form.submitted = true;
                $timeout(function () {
                        $scope.loading = false;
                }, 500);
            }
        }
    /***********update profile end***************/
})
.controller('LoginCtrl', function($scope, $formSubmit, localStorageService, $state, $timeout){
    var userId      = localStorageService.get('userId');
    var userName    = localStorageService.get('userName');
    /*****************LogIn Start********************/
    console.log(userId);
    if(userId == null){
        $scope.loginForm = function(form) { 
            $scope.loading = true;
            if ($scope.login_form.$valid) {
                // Submit as normal 
                console.log(form.captchaCode);
                var captchaCode = form.captchaCode;
                console.log(md5(captchaCode));
                console.log($scope.resCd);
                if($scope.resCd == md5(captchaCode)){
                    var loginParam  = {
                            'email'     : form.email,
                            'password'  : form.password
                        };
                    
                    $formSubmit.userLoginFactory(loginParam).then(function (regResponseData) {
                        console.log(regResponseData);
                        if(regResponseData.responseCode == 200){
                            var userData = regResponseData.userParam;

                            localStorageService.set('userId', userData.id);
                            localStorageService.set('userName', userData.name);
                            localStorageService.set('email', userData.email);
                            
                            $state.go('dashboards.index');
                            $scope.loading = false;
                        } else if(regResponseData.responseCode == 201){
                            $scope.Message = regResponseData.responseMessage;
                            form.email = '';
                            form.password = '';
                            
                            $timeout(function () {
                                    $scope.loading = false;
                            }, 500);
                        } else if(regResponseData.responseCode == 203){
                            var arrData = regResponseData.responseMessage;
                            var mess = [];
                            for (var i = 0; i < arrData.length; ++i){
                               mess[i] = arrData[i].responseMessage; 
                            }
                            var arrToStr = mess.toString();
                            $scope.Message = arrToStr;
                            form.email = '';
                            form.password = '';
                            $timeout(function () {
                                    $scope.loading = false;
                            }, 500);
                        }
                    }, function (regResponseData) {
                        console.log('error');
                    });
                } else {
                    $scope.Message = 'Invalid captcha code!';
                    $scope.loading = false;
                }
                
            } else {
                $scope.login_form.submitted = true;
                $timeout(function () {
                        $scope.loading = false;
                }, 500);
            }
        }
    }
    if(userId != null){
        $state.go('dashboards.index');
    } 
    /***********LogIn end ***************/
})
.controller('LogoutCtrl', function($scope, localStorageService, $location){
    $scope.goTo = function(){
        //Session.clear();
        var userId      = localStorageService.set('userId', null);
        var userName      = localStorageService.set('userName', null);
        var email      = localStorageService.set('email', null);
        $location.path('/login');
    }
})
.controller('UpdateProfileCtrl', function($scope, $formSubmit, localStorageService, $location){
    /***********update profile start***************/
        var userId      = localStorageService.get('userId');
        var userName    = localStorageService.get('userName');
        var email       = localStorageService.get('email');
        console.log(userId, userName, email);
        $scope.name     = userName;
        $scope.email    = email;
        $scope.userId   = userId;
        if(userId != null){
            $scope.updateForm = function() { 
                if ($scope.update_form.$valid) {
                    // Submit as normal
                    //console.log($scope.password);
                    var password = '';
                    if($scope.password != null){
                        password = $scope.password;
                    } else {
                        password = '';
                    }
                    var updateParam = {
                            'userId'    : $scope.userId,
                            'name'      : $scope.name,
                            'email'     : $scope.email,
                            'password'  : password
                    };
                    

                    $formSubmit.profileUpdateFactory(updateParam).then(function (regResponseData) {
                        console.log(regResponseData);
                        if(regResponseData.responseCode == 200){
                            $scope.succMessage = regResponseData.responseMessage;
                        } else if(regResponseData.responseCode == 201){
                            $scope.errorMessage = regResponseData.responseMessage;
                        } else if(regResponseData.responseCode == 203){
                            var arrData = regResponseData.responseMessage;
                            var mess = [];
                            for (var i = 0; i < arrData.length; ++i){
                               mess[i] = arrData[i].responseMessage; 
                            }
                             var arrToStr = mess.toString();
                            // console.log(arrToStr);
                            $scope.errorMessage = arrToStr;
                        }
                    }, function (regResponseData) {
                        console.log('error');
                    });
                } else {
                    $scope.update_form.submitted = true;
                    
                }
            }
        } else if (userId == null){
            $location.path('/login');
        }
        
    /***********update profile end***************/
});

