var csvModule = angular.module('csv.module', []);

/**********route config start**********/
csvModule.config(function ($stateProvider) {
    $stateProvider
    .state('csv', {
        abstract: true,
        url: "/csv",
        templateUrl: "views/common/content.html"
    })
    .state('csv.list', {
        url: "/list",
        templateUrl: "views/csv/list.html",
        controller : 'ListCsvCtrl',
        data: { pageTitle: 'Csv List' },
        resolve: {
              loadPlugin: function ($ocLazyLoad) {
                  return $ocLazyLoad.load([
                      {
                          serie: true,
                          files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                      },
                      {
                          serie: true,
                          name: 'datatables',
                          files: ['js/plugins/dataTables/angular-datatables.min.js']
                      },
                      {
                          serie: true,
                          name: 'datatables.buttons',
                          files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                      },
                      {
                            serie: true,
                            name: 'angular-ladda',
                            files: ['js/plugins/ladda/spin.min.js', 'js/plugins/ladda/ladda.min.js', 'css/plugins/ladda/ladda-themeless.min.css','js/plugins/ladda/angular-ladda.min.js']
                      },
                      {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                      },
                      {
                          name: 'oitozero.ngSweetAlert',
                          files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                      }
                  ]);
              }
          }
    })
    .state('csv.upload', {
        url: "/upload",
        templateUrl: "views/csv/add.html",
        controller : 'uploadCsvCtrl',
        data: { pageTitle: 'Upload CSV' }
    })
});
/**********route config end**********/


