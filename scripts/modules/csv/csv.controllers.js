csvModule
.controller('uploadCsvCtrl', function($scope, $csvForm, ENV, localStorageService, $location, $state, $http){
    var userId      = localStorageService.get('userId');
    if(userId != null){
        $scope.uploadFile = function(){
            var file = $scope.myFile;
            if(file){
                ext = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase(); 
                if(ext === 'csv'){
                    var uploadUrl = ENV.apiserverRoot+'/'+ENV.uploadCSVApiEntryPoint;
                    var fd = new FormData();
                    fd.append('file', file);

                    $http.post(uploadUrl,fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    })
                    .success(function(){
                        $scope.succMessage = 'CSV file upload successfully.';
                        $state.go('csv.list');
                        //console.log("success!!");
                    })
                    .error(function(){
                        $scope.errorMessage = 'CSV file upload not successfully.';
                        //console.log("error!!");
                    });
                } else {
                    $scope.errorMessage = 'Please select a csv file!';
                }
            }
        };
    } else {
        $location.path('/login');
    }
})
.controller('ListCsvCtrl', function($scope, $csvForm, $timeout,  localStorageService, $location) {
        var userId = localStorageService.get('userId');
        if (userId != null) {
            $csvForm.getCsvFactory().then(function(csvResponseData) {
                console.log(csvResponseData);
                if (csvResponseData.responseCode == 200) {
                    $scope.csvObjData = csvResponseData.csvObj
                } else if (csvResponseData.responseCode == 201) {
                    $scope.errorMessage = csvResponseData.responseMessage;
                }
            }, function(csvResponseData) {
                console.log('error');
            });
        } else if (userId == null) {
            $location.path('/login');
        }
    }); 