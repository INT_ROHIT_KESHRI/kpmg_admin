kpmgModule.factory('$cryptoAdapter', function($http, $q, $log, $rootScope, $interval, $timeout) {

    var mode = CryptoJS.mode.ECB;
    var padding = CryptoJS.pad.Pkcs7;
    var Cipher = CryptoJS.AES;
    //console.log(CryptoJS);
    function addBlockLoader() {
        document.getElementById('function_popbg').style.display = 'block';
    }
    function removeBlockLoader() {
        document.getElementById('function_popbg').style.display = 'none';
    }
    function saltGen() {
        var salt = CryptoJS.lib.WordArray.random(128/8).toString();
        return salt;
    }

    function privateKeyGen(salt) {
        //var deferred  = $q.defer();
        var hasherConfig    = {
            hasher: CryptoJS.algo.SHA1,
            keySize: 4,
            iterations: 1000
        }
        var key128Bits = CryptoJS.PBKDF2('I!ndu(sNet!#Ra*Ys*Imaging', salt, hasherConfig);
        return key128Bits;
    }

    function dataEncode(salt, plainValue,textType ='text') {
        if(textType==="json"){
            var plainText = JSON.stringify(plainValue);
        }else{
            var plainText = plainValue;
        }
        //var salt = saltGen();
        var key = privateKeyGen(salt);
        var encryprtedData = Cipher.encrypt(plainText, key, {
            mode: mode,
            padding: padding
        });
        var postData = {
            cryptoSalt : salt,
            encData : encryprtedData.toString()
        };
        return postData;
    }

    function dataDecode(salt, encData,textType ='text') {
        var key = privateKeyGen(salt);
        var decryptedData = Cipher.decrypt(encData, key, {
            mode: mode,
            padding: padding
        });
        //var plainText = hex2a(decryptedData.toString());
        var plainText = decryptedData.toString(CryptoJS.enc.Utf8);
        if(textType==="json"){
            return JSON.parse(plainText);
        }else{
             return plainText;
        }

       //
    }
    /*function dataDecode (salt, encData) {
        var deferred  = $q.defer();
        privateKeyGen(salt).then(function (key) {
            console.log(key);
            var decryptedData = Cipher.decrypt(encData, key, {
                mode    : mode,
                padding : padding
            });
            var plainText = decryptedData.toString(CryptoJS.enc.Utf8);
            deferred.resolve(JSON.parse(plainText));
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }*/
    function hex2a(hexx) {
        var hex = hexx.toString();//force conversion
        var str = '';
        for (var i = 0; i < hex.length; i += 2)
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        return str;
    }

   /*
   * Expose the public function
   */
   return {
       dataEncode : dataEncode,
       dataDecode : dataDecode,
       saltGen : saltGen,
       addBlockLoader:addBlockLoader,
       removeBlockLoader:removeBlockLoader,

   };
});
