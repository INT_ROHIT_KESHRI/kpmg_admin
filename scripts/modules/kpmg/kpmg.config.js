"use strict";
/*#constant:start#*/ 
kpmgModule
    .constant("ENV", {
        "apiserverRoot"         : "http://150.129.179.174:4000",
        //"apiserverRoot"         : "http://raysimaging.com:3000",
        //"apientryPoint"                 : "rays_backend",
        "regUserapientryPoint"  : "api/admin/auth/registerUser",
        "loginApiEntryPoint"    : "api/admin/auth/loginAdmin",
        "updateApiEntryPoint"   : "api/admin/auth/updateAdmin",
        "getUserApiEntryPoint"  : "api/admin/auth/getUser",

        "uploadCSVApiEntryPoint": "api/admin/authed/uploadCSV",
        //"addGalleryApiEntryPoint"         : "masters/addGallery",
        //"deleteGalleryApiEntryPoint": "masters/deleteGallery",
        "listCsvApiEntryPoint"  : "api/admin/authed/listCsv",

        //"getDashboardApi"               : "reports/getDashBoardReport"
    });
/*#constant:end#*/
