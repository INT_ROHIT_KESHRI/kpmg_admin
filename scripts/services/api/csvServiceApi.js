csvModule
    .factory('$csvForm', function($http, ENV, $q) {
        function getCsvFactory(){
            var def = $q.defer();
            //*********************************//
            $http({
                method  : 'GET',
                url     : ENV.apiserverRoot+'/'+ENV.listCsvApiEntryPoint,
                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
            }).success(function(data) {
                def.resolve(data);
            }).error(function(data){
                def.reject(data);
            }) ;

            return def.promise;
            //*******************************************//
        }

        
        return {
            getCsvFactory: getCsvFactory
        };
    });