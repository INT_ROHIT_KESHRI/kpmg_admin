userModule
.factory('$formSubmit', function ($http, ENV, $q) {
	function registerUserFactory(regParam){
        console.log(regParam);
        var def = $q.defer();
        
        $http({
            method  : 'POST',
            url     : ENV.apiserverRoot+'/'+ENV.regUserapientryPoint,
            data    : 'name='+regParam.name+'&email='+regParam.email+'&password='+regParam.password, //forms user object
            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
        }).success(function(data) {
            //console.log(data);
            def.resolve(data);
        }).error(function(data){
            //console.log(data);
            def.reject(data);
        }) ;
        return def.promise;
    }
    function userLoginFactory(loginParam){
        var def = $q.defer();
        //*********************************//
        $http({
            method  : 'POST',
            url     : ENV.apiserverRoot+'/'+ENV.loginApiEntryPoint,
            data    : 'email='+loginParam.email+'&password='+loginParam.password, //forms user object
            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
        }).success(function(data) {
            def.resolve(data);
        }).error(function(data){
            def.reject(data);
        }) ;

        return def.promise;
        //*******************************************//
    }
    // function forgotPassData (encryptedEmail){
    //     var def = $q.defer();
    //     //*********************************//
    //     $http({
    //         method  : 'POST',
    //         url     : ENV.apiserverRoot+'/'+ENV.apientryPoint+'/'+ENV.forgotPassApiEntryPoint,
    //         data    : 'email='+encryptedEmail , //forms user object
    //         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
    //     }).success(function(data) {
    //         def.resolve(data);
    //     }).error(function(data){
    //         def.reject(data);
    //     }) ;

    //     return def.promise;
    //     //*******************************************//
    // }
    function profileUpdateFactory(updateParam){
        var def = $q.defer();
        //*********************************//
        $http({
            method  : 'POST',
            url     : ENV.apiserverRoot+'/'+ENV.updateApiEntryPoint,
            data    : 'userId='+updateParam.userId+'&name='+updateParam.name+'&email='+updateParam.email+'&password='+updateParam.password, //forms user object
            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
        }).success(function(data) {
            ///console.log(data);
            def.resolve(data);
        }).error(function(data){
            //console.log(data);
            def.reject(data);
        }) ;

        return def.promise;
        //*******************************************//
    }
    
    return {
        registerUserFactory    : registerUserFactory,
        userLoginFactory       : userLoginFactory,
        //forgotPassData         : forgotPassData,
        profileUpdateFactory   : profileUpdateFactory
	};
});