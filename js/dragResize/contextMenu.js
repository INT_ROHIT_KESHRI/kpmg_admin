// If the document is clicked somewhere
$(document).bind("mousedown", function (e) {
    // If the clicked element is not the menu
    if (!$(e.target).parents(".custom_menu").length > 0) {
       $(".custom_menu").hide(100);
       $('.container').css({'border':'1px solid red'});
    }
});



var zArr    =   [];
function makeMove(direction){
  zArr    =   [];
  $('.parentDiv').children().each(function(){
        var objId      =   $(this).attr('id');
        var objZid   =   $(this).css('z-index');
        zArr.push({'id':objId,'zIdx':objZid});
  });
  $(".custom_menu").hide(100);
  switchIndex(direction);
}

function switchIndex(direc){
    getSortedKeys(zArr);
    for(var i=0; i < zArr.length; i++){
        var objId = zArr[i].id;
       if(objId === selectedObj){
         if(direc == 'front'){
           $('#'+objId).css({'z-index': Number(zArr[zArr.length-1].zIdx)+1});
         }else if(direc == 'back'){
           $('#'+objId).css({'z-index': 1});
         }else if(direc == 'fwd'){
           $('#'+objId).css({'z-index': Number(zArr[i].zIdx)+1});
         }else{
           if(Number(zArr[i].zIdx) >= 1){
              $('#'+objId).css({'z-index': Number(zArr[i].zIdx)-1});
           }
         }
         break;
       }
    }
}

function getSortedKeys(obj) {
  obj.sort(function(a,b) {
    return a.zIdx - b.zIdx;
  });
}
