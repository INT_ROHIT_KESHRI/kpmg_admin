var count=0;
//var posArr=[];
var editableDiv;
var container;
var closeBtn;
var selectedObj;
var positionType = [];
var lastCreatedElementIndex;
var dialog = $('<div id="dialog-confirm" title="Delete the element?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>The item will be permanently deleted and cannot be recovered. Are you sure?</p></div>');

function addListener(positionType){
	//$("input.custom-radio").checkboxradio();
	$("#dialog-type").dialog({
		autoOpen: false,
		resizable: false,
		height: "auto",
		width: 400,
		modal: true,
		buttons: {
			"Create": function() {
				var type = $('input[type="radio"].custom-radio:checked').val();
				getDimension(type, positionType);
				$(this).dialog("close");
			},
			Cancel: function() {
				$(this).dialog("close");
			}
		}
	});
}

function saveListener(){
	var posArr = [];
	var parentDivWidth 	= $('.parentDiv').width();
	var parentDivHeight = $('.parentDiv').height();
	var elem 			= '';
	var height 			= '';
	var width 			= '';
	var positionNo 		= '';
	var positionTypeVal = '';

	objArr    =   [];
	$('.parentDiv').children().each(function(){
	    var objId    =   $(this).attr('id');
	    objArr.push({'id':objId});
	});
	// console.log(positionType);
	// console.log(objArr);
	// console.log(positionType.length);
	for(var i=1; i<=positionType.length-1; i++){
		var dragId = 'draggable_'+i;
		elem 		= $('#'+dragId);
		var draggableId = $('#draggable_'+i);
		// console.log($('#draggable_'+i).length);
		// console.log(draggableId.length);
		// console.log('parent loop',i);
		if($('#draggable_'+i).length){
			//console.log('child loop',i);
			height 			= elem.height();
			width 			= elem.width();
			positionNo 		= i;
			positionTypeVal = positionType[i];
			ratioval 		= height/width;
			var offsetL 		= elem.offset().left - elem.parent().offset().left;
			var offsetT 		= elem.offset().top - elem.parent().offset().top;

			var percentLeft 	= offsetL/parentDivWidth * 100;
			var percentTop 		= offsetT/parentDivHeight *100;

			var percentHeight 	= height/parentDivHeight *100;
			var percentWidth 	= width/parentDivWidth *100;
			//console.log('height:',height,'width:',width, 'percentHeight', percentHeight, 'percentWidth', percentWidth, parentDivHeight, parentDivWidth);
			posArr.push({
				'positionNo' 	: positionNo,
				'left'			: offsetL,
				'top'			: offsetT,
				'height'		: height,
				'width' 		: width,
				'parentDivWidth': parentDivWidth,
				'parentDivHeight':parentDivHeight,
				'pcLeft' 		: percentLeft,
				'pcTop' 		: percentTop,
				'pcHeight'		: percentHeight,
				'pcWidth' 		: percentWidth,
				'positionType' 	: positionTypeVal,
				'ratio'			: ratioval
			});
		}
		if(i == positionType.length-1){
			return posArr;
		}
	}
	//console.log(posArr);
}

function getListener(obj, childDivNo){
	if(obj.length > 0){
		for(var i=0; i<obj.length; i++){
			var count = obj[i]['positionNo'];
			editableDiv = $('<div id="editable_'+count+'" contenteditable=true>'+obj[i]['positionType']+'</div>');
			container = $('<div class="ui-widget-content container ui-widget-content-'+obj[i]['positionType']+'"  id="draggable_'+count+'"></div>');
			closeBtn = $('<a href="javascript:void(0)" class="close"><img src="js/dragResize/img/close.png" width="25" height="25" alt=""/></a>');
			$('.parentDiv').append(container);
			positionType[count] = obj[i]['positionType'];

			/*Dialog*/
			dialog.dialog({
				autoOpen: false,
				resizable: false,
				height: "auto",
				width: 400,
				modal: true,
				buttons: {
					"Delete": function() {
						$("#"+target).remove();
						count--;
						$(this).dialog("close");
					},
					Cancel: function() {
						$(this).dialog("close");
					}
				}
			});

			/*Parent DIV Draggable & Resizable*/
			container
			.append(closeBtn, editableDiv)
			.css({
				'top' : obj[i]['top']+'px',
				'left' : obj[i]['left']+'px',
				'position' : 'absolute',
				'border' : '1px solid red',
				'width': obj[i]['width'],
				'height': obj[i]['height'],
				'z-index': count
			})
			.resizable({
				minHeight: obj[i]['height'],
				minWidth: obj[i]['width'],
				aspectRatio: obj[i]['ratio'],
				containment: ".parentDiv"
			})
			.draggable({
				containment: ".parentDiv"
			});

			/*Click event for close button*/
			closeBtn
			.on('click', function(e){
				target = $(this).parent().attr('id');
				dialog.dialog("open");
			});
		}
	} else {
		 // $( ".container" ).remove();
		  $( "div" ).remove( ".container" );
	}
	totalCount = obj.length;
	return totalCount;
}

function getDimension(type, positionType){
	var obj = {};
	if(type == 'l'){
		obj = {
			'height':90,
			'width':160,
			'ratio':16/9
		};
	}else{
		obj = {
			'height':160,
			'width':90,
			'ratio':9/16
		};
	}
	addElement(obj, positionType);
}

function addElement(obj, type){
	if($('.parentDiv').children('.container').length > 0){
		var lastCreatedElementID = $('.parentDiv').children('.container').last().attr('id');
		lastCreatedElementIndex = lastCreatedElementID.substr(lastCreatedElementID.lastIndexOf("_")+1);
	} else {
		lastCreatedElementIndex = 0;
	}
	count = lastCreatedElementIndex;
	count++;

	editableDiv = $('<div id="editable_'+count+'" contenteditable=true>'+type+'</div>');
	container = $('<div class="container ui-widget-content ui-widget-content-'+type+'" id="draggable_'+count+'"></div>');
	closeBtn = $('<a href="javascript:void(0)" class="close"><img src="js/dragResize/img/close.png" width="25" height="25" alt=""/></a>');
	$('.parentDiv').append(container);
	positionType[count] = type;
	console.log(positionType);
	/*Dialog*/
	dialog.dialog({
		autoOpen: false,
		resizable: false,
		height: "auto",
		width: 400,
		modal: true,
		buttons: {
			"Delete": function() {
				$("#"+target).remove();
				//count--;
				$(this).dialog("close");
			},
			Cancel: function() {
				$(this).dialog("close");
			}
		}
	});

	/*Parent DIV Draggable & Resizable*/
	container
	.append(closeBtn, editableDiv)
	.css({
		'top' : ($('.parentDiv').height()- obj.height)/2,
		'left' : ($('.parentDiv').width()- obj.width)/2,
		'position' : 'absolute',
		'border' : '1px solid red',
		'width': obj.width,
		'height': obj.height,
		'z-index': count
	})
	.resizable({
		minHeight: obj.height,
		minWidth: obj.width,
		aspectRatio: obj.ratio,
		containment: ".parentDiv"
	})
	.draggable({
		containment: ".parentDiv"
	});

	/*Click event for close button*/
	closeBtn
	.on('click', function(e){
		target = $(this).parent().attr('id');
		dialog.dialog("open");
	});
}

/*Final template preview*/
function templatePreviewListener(obj, imagePath){
	console.log(obj);
	if(obj.length > 0){
		for(var i=0; i<obj.length; i++){
			var count = obj[i]['positionNo'];
			var templateId = obj[i]['templateId'];
			editableDiv = $('<div id="editable_'+count+'" contenteditable=true>'+obj[i]['textValue']+'</div>');

			if(obj[i]['positionType'] == 'img'){
				container = $('<div class="ui-widget-content container"><img src="'+imagePath+obj[i]['link']+'" height="'+obj[i]['height']+'" width="'+obj[i]['width']+'"></div>');
			} else {
				container = $('<div class="ui-widget-content container"></div>');
			}
			$('.parentDiv'+templateId).append(container);	
			/*Parent DIV Draggable & Resizable*/ 
			container
			.append(editableDiv)
			.css({
				'top' : obj[i]['top']+'px',
				'left' : obj[i]['left']+'px',
				'position' : 'absolute',
				'border' : '1px solid red',
				'width': obj[i]['width'],
				'height': obj[i]['height'],
				'z-index': count
			});
		}
	} else {
		 // $( ".container" ).remove();
		  $( "div" ).remove( ".container" );
	}
}

